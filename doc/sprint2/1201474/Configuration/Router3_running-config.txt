!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX101724N0-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.18.80.131 255.255.255.128
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.441
 encapsulation dot1Q 441
 ip address 172.18.84.129 255.255.255.240
!
interface FastEthernet1/0.442
 encapsulation dot1Q 442
 ip address 172.18.84.1 255.255.255.192
!
interface FastEthernet1/0.443
 encapsulation dot1Q 443
 ip address 172.18.83.65 255.255.255.192
!
interface FastEthernet1/0.444
 encapsulation dot1Q 444
 ip address 172.18.84.193 255.255.255.224
!
interface FastEthernet1/0.445
 encapsulation dot1Q 445
 ip address 172.18.85.33 255.255.255.224
!
interface Vlan1
 no ip address
 shutdown
!
ip classless
ip route 172.18.84.128 255.255.255.192 172.18.80.129 
ip route 172.18.84.128 255.255.255.192 172.18.80.130 
ip route 172.18.84.128 255.255.255.192 172.18.80.132 
ip route 172.18.84.0 255.255.255.192 172.18.80.129 
ip route 172.18.84.0 255.255.255.192 172.18.80.130 
ip route 172.18.84.0 255.255.255.192 172.18.80.132 
ip route 172.18.83.64 255.255.255.192 172.18.80.129 
ip route 172.18.83.64 255.255.255.192 172.18.80.130 
ip route 172.18.83.64 255.255.255.192 172.18.80.132 
ip route 172.18.84.192 255.255.255.224 172.18.80.129 
ip route 172.18.84.192 255.255.255.224 172.18.80.130 
ip route 172.18.84.192 255.255.255.224 172.18.80.132 
ip route 172.18.85.32 255.255.255.224 172.18.80.129 
ip route 172.18.85.32 255.255.255.224 172.18.80.130 
ip route 172.18.85.32 255.255.255.224 172.18.80.132 
ip route 0.0.0.0 0.0.0.0 15.203.48.118 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

