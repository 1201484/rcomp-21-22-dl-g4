!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Router
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2811/K9 sn FTX10171XC7-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 172.18.30.130 255.255.255.128
 duplex auto
 speed auto
!
interface FastEthernet0/1
 no ip address
 duplex auto
 speed auto
!
interface FastEthernet1/0
 no ip address
!
interface FastEthernet1/0.436
 encapsulation dot1Q 436
 ip address 172.18.85.1 255.255.255.224
!
interface FastEthernet1/0.437
 encapsulation dot1Q 437
 ip address 172.18.83.193 255.255.255.192
!
interface FastEthernet1/0.438
 encapsulation dot1Q 438
 ip address 172.18.81.1 255.255.255.128
!
interface FastEthernet1/0.439
 encapsulation dot1Q 439
 ip address 172.18.85.65 255.255.255.240
!
interface FastEthernet1/0.440
 encapsulation dot1Q 440
 ip address 172.18.85.81 255.255.255.240
!
interface Vlan1
 no ip address
 shutdown
!
router rip
!
ip classless
ip route 172.18.80.0 255.255.255.128 172.18.80.129 
ip route 172.18.81.128 255.255.255.128 172.18.80.129 
ip route 172.18.82.0 255.255.255.128 172.18.80.129 
ip route 172.18.83.0 255.255.255.192 172.18.80.129 
ip route 172.18.83.64 255.255.255.192 172.18.80.131 
ip route 172.18.84.0 255.255.255.192 172.18.80.131 
ip route 172.18.84.128 255.255.255.192 172.18.80.131 
ip route 172.18.85.32 255.255.255.224 172.18.80.131 
ip route 172.18.82.128 255.255.255.128 172.18.80.132 
ip route 172.18.83.128 255.255.255.192 172.18.80.132 
ip route 172.18.84.64 255.255.255.192 172.18.80.129 
ip route 172.18.84.192 255.255.255.224 172.18.80.131 
ip route 172.18.84.224 255.255.255.224 172.18.80.132 
ip route 172.18.85.96 255.255.255.240 172.18.80.132 
ip route 172.18.85.112 255.255.255.240 172.18.80.132 
ip route 0.0.0.0 0.0.0.0 172.18.80.133 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

