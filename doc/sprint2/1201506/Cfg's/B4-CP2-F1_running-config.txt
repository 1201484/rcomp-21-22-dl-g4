!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Switch
!
!
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface FastEthernet0/1
 switchport trunk allowed vlan 1-429,451-1001
!
interface FastEthernet1/1
 switchport mode trunk
!
interface FastEthernet2/1
 switchport access vlan 447
 switchport mode access
!
interface FastEthernet3/1
 switchport access vlan 449
 switchport mode access
!
interface Vlan1
 no ip address
 shutdown
!
!
!
!
line con 0
!
line vty 0 4
 login
line vty 5 15
 login
!
!
!
!
end

