RCOMP 2021-2022 Project - Sprint 2 review
=========================================
### Sprint master: 1201484 ###
# 1. Sprint's backlog #
T.2.1 - Development of a layer two and layer three Packet Tracer simulation for building one, encompassing the campus backbone. Integration of every member’s Packet Tracer simulation into a single simulation. T.2.2 - Development of a layer two and layer three Packet Tracer simulation for building two, encompassing the campus backbone. T.2.3 - Development of a layer two and layer three Packet Tracer simulation for building three, encompassing the campus backbone.T.2.4 - Development of a layer two and layer three Packet Tracer simulation for building four, encompassing the campus backbone.
# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

(Examples)
## 2.1. 1201474 -  Development of a layer two and layer three Packet Tracer simulation for building one, encompassing the campus backbone. Integration of every member’s Packet Tracer simulation into a single simulation. #
### Totally implemented with issues. ###
There is no connectivity between all routers.
## 2.2. 1201484 -Development of a layer two and layer three Packet Tracer simulation for building two, encompassing the campus backbone. #
### Totally implemented with issues. ###
There is no connectivity between all routers.
## 2.3. 1201461 - Development of a layer two and layer three Packet Tracer simulation for building three, encompassing the campus backbone. #
### Totally implemented with issues. ###
There is no connectivity between all routers.
## 2.4. 1201506 - Development of a layer two and layer three Packet Tracer simulation for building four, encompassing the campus backbone. #
### Totally implemented with issues. ###
There is no connectivity between all routers.


