RCOMP 2021-2022 Project - Sprint 1 planning
===========================================
### Sprint master: 1201474 ###

# 1. Sprint's backlog #

T.3.1 Update the building1.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 1.

T.3.2 Update the building2.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 2.Final integration of each member’s Packet Tracer simulation into a
single simulation (campus.pkt).

T.3.3 Update the building3.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 3.

T.3.4 Update the building4.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 4.


# 2. Technical decisions and coordination #

- All member used Cisco Packet Tracer version 8.0.0.0212.

- Devices naming convencion follows the rule "DeviceName"-"BuildingName"-"Floor" i.e.(ICC-B2-F0).

- The main output is the Packet Tracer simulation file for the corresponding building, it should be named BuildingN.pkt, with N being the number which identifies the building.


- The **OSPF area ids** are:

  Backbone OSPF area id-0
  Building1 OSPF area id-1
  Building2 OSPF area id-2
  Building3 OSPF area id-3
  Building4 OSPF area id-4


-	The **VoIP prefixes** are:
     - Building 1:
     Floor 0: 1-000
     Floor 1: 1-001
     - Building 2:
     Floor 0: 2-000
     Floor 1: 2-001
     - Building 3:
     Floor 0: 3-000
     Floor 1: 3-001
     -Building 4:
     Floor 0: 4-000
     Floor 1: 4-001

  
  
  * DNS Domain
  Building 1: DNS domain name: rcomp-21-22-dl-g4 DNS server name- ns.rcomp-21-22-dl-g4
  Building 2: DNS domain name: building-2.rcomp-21-22-dl-g4 DNS server name- ns.building-2.rcomp-21-22-dl-g4
  Building 3: DNS domain name: building-3.rcomp-21-22-dl-g4 DNS server name- ns.building-3.rcomp-21-22-dl-g4
  Building 4: DNS domain name: building-4.rcomp-21-22-dl-g4 DNS server name- ns.building-4.rcomp-21-22-dl-g4
  
  
  
# 3. Subtasks assignment #
(For each team member (sprint master included), the description of the assigned subtask in sprint 3)

#### Example: ####
  * 1201474 - T.3.1- Update the building1.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 1.
  * 1201484 - T.3.2- Update the building2.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 2.Final integration of each member’s Packet Tracer simulation into a  single simulation (campus.pkt).
  * 1201461 - T.3.3- Update the building3.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 3.
  * 1201506 - T.3.4- Update the building4.pkt layer three Packet Tracer simulation from the previous sprint, to include the described features in this sprint for building 4.
