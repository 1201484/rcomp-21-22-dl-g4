RCOMP 2021-2022 Project - Sprint 1 review
=========================================
### Sprint master: 1201461 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
T.1.1 Development of a structured cabling project for building 1, encompassing the campus backbone.
T.1.2 Development of a structured cabling project for building 2.
T.1.3 Development of a structured cabling project for building 3.
T.1.4 Development of a structured cabling project for building 4.

* The tasks 1.1 to 1.4 are divided in the following scenarios:
  * Demonstration of calculations regarding the number of network outlets for each room. 
  * Network outlets deployment schematic plan (including outlets for wireless access points) and justification comments. 
  * Cross-connects deployment schematic plan and justification comments. 
  * Cable pathways deployment schematic plan and justification comments. 
  * Hardware inventories, including total cable lengths by cable type, appropriate type patch panels, network outlets, telecommunication enclosures of suitable size.

# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * Totally implemented with no issues
  * Totally implemented with issues
  * Partially implemented with no issues
  * Partially implemented with issues

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

## 2.1. 1201474 - Development of a structured cabling project for building 1, encompassing the campus backbone.#
### Demonstration of calculations regarding the number of network outlets for each room: ###
Totally implemented with no issues.

### Network outlets deployment schematic plan (including outlets for wireless access points) and justification comments: ### 
Totally implemented with no issues.

### Cross-connects deployment schematic plan and justification comments: ### 
Totally implemented with no issues.

### Cable pathways deployment schematic plan and justification comments: ### 
Totally implemented with no issues.

### Hardware inventories, including total cable lengths by cable type, appropriate type patch panels, network outlets, telecommunication enclosures of suitable size: ### 
Totally implemented with no issues.


## 2.2. 1201484 - Development of a structured cabling project for building 2. #
### Demonstration of calculations regarding the number of network outlets for each room: ###
Totally implemented with no issues.

### Network outlets deployment schematic plan (including outlets for wireless access points) and justification comments: ### 
Totally implemented with no issues.

### Cross-connects deployment schematic plan and justification comments: ### 
Totally implemented with no issues.

### Cable pathways deployment schematic plan and justification comments: ### 
Totally implemented with no issues.

### Hardware inventories, including total cable lengths by cable type, appropriate type patch panels, network outlets, telecommunication enclosures of suitable size: ### 
Totally implemented with no issues.


## 2.3. 1201461 - Development of a structured cabling project for building 3. #
### Demonstration of calculations regarding the number of network outlets for each room: ###
Totally implemented with no issues.

### Network outlets deployment schematic plan (including outlets for wireless access points) and justification comments: ### 
Totally implemented with no issues.

### Cross-connects deployment schematic plan and justification comments: ### 
Totally implemented with no issues.

### Cable pathways deployment schematic plan and justification comments: ### 
Totally implemented with no issues.

### Hardware inventories, including total cable lengths by cable type, appropriate type patch panels, network outlets, telecommunication enclosures of suitable size: ### 
Totally implemented with no issues.


## 2.4. 1201506 - Development of a structured cabling project for building 4. #
### Demonstration of calculations regarding the number of network outlets for each room: ###
Totally implemented with no issues.

### Network outlets deployment schematic plan (including outlets for wireless access points) and justification comments: ### 
Totally implemented with no issues.

### Cross-connects deployment schematic plan and justification comments: ### 
Totally implemented with no issues.

### Cable pathways deployment schematic plan and justification comments: ### 
Totally implemented with no issues.

### Hardware inventories, including total cable lengths by cable type, appropriate type patch panels, network outlets, telecommunication enclosures of suitable size: ### 
Totally implemented with no issues.

