RCOMP 2021-2022 Project - Sprint 1 planning
===========================================
### Sprint master: 1201461 ###
(This file is to be created/edited by the sprint master only)
# 1. Sprint's backlog #
T.1.1 Development of a structured cabling project for building 1, encompassing the campus backbone.
T.1.2 Development of a structured cabling project for building 2.
T.1.3 Development of a structured cabling project for building 3.
T.1.4 Development of a structured cabling project for building 4.

# 2. Technical decisions and coordination #

  * Utilizámos cabos fibra monomode para a ligação entre os edifícios do campus;
  * Considerámos que todos os compartimentos estão preparados para receber quaisquer equipamentos de utilizador de modo que existe uma tomada a menos de 3 metros;
  * Nas ligações entre edifícios optámos por usar 2 tipos de cabo de modo a prevenir alguma eventualidade futura;
  * Utilizámos cabos CAT6A para as ligações entre CP's e outlets e HC's e outlets;
  * Respeitámos todas as regras relativas ao tamanho de cabo usado e de outlets por 10 m^2.

# 3. Subtasks assignment #
  
  * 1201474 - Development of a structured cabling project for building 1, encompassing the campus backbone.
  * 1201484 - Development of a structured cabling project for building 2.
  * 1201461 - Development of a structured cabling project for building 3.
  * 1201506 - Development of a structured cabling project for building 4.
