RCOMP 2021-2022 Project - Sprint 1 - Member 1201506 folder
===========================================
(This folder is to be created/edited by the team member 1201506 only)

#### This is just an example for a team member with number 1201506 ####
### Each member should create a folder similar to this, matching his/her number. ###
The owner of this folder (member number 1201506) will commit here all the outcomes (results/artifacts/products) of his work during sprint 1. This may encompass any kind of standard file types.
