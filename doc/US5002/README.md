# US5002
=======================================


# 1. Requisitos

*As Project Manager, I want that the team start developing the output communication module of the AGV digital twin to update its status on the "AGVManager".*

# 2. Análise

*Specifications:*

*As Project Manager, I want that the team start developing the output communication module of the AGV digital twin to update its status on the "AGVManager".*



# 3. Design

## 3.1. Realização da Funcionalidade



## 3.2. Padrões Aplicados

*Para esta user storie seguimos o protocólogo do ficheiro SPOMS, usámos os sockets para enviar e aceitar requests do AGVManager para o AGV Digital Twin, recorremos ainda à base de dados através do backoffice e da us 5001 tal como a 4001 para mudar o status do AGVManager*
*Ainda no âmbito das user stories de rcomp criámos servers no docker do isep para podermos ter a comunicação de AGVs entre vários computadores.*

## 3.4. Testes

**Não implementámos testes pois esta user storie refere-se à comunicação entre servers e clients no dominío de redes da computação.**


# 5. Integração/Demonstração

*Tivémos o cuidado de assegurar que todas as funcionalidades se complementam pois algumas delas dependem das outras.
Neste sprint quase todas as funcionalidades são desempenhadas por um sales clerk desta maneira elas estão interligadas.*

# 6. Observações


*A implementação por nós pensada e realizada parece assentar naquilo que seria o mais funcioonal para a funcionalidade
pedida.*


