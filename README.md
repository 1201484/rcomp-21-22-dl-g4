RCOMP 2021-2022 Project repository template
===========================================
# 1. Team members (update this information please) #
  * 1201461 - Beatriz Borges 
  * 1201474 - Érica Lopes
  * 1201484 - Eduardo Muro 
  * 1201506 - Guilherme Cunha 

Any team membership changes should be reported here, examples:

Member 8888888 ({First and last name}) has left the team on 2022-03-20

Member 7777777 ({First and last name}) has entered the team on 2022-04-5
# 2. Sprints #
  * [Sprint 1](doc/sprint1/)
  * [Sprint 2](doc/sprint2/)
  * [Sprint 3](doc/sprint3/)

